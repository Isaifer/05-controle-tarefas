package com.mastertech.controletarefas.service;

import com.mastertech.controletarefas.persistence.Project;
import com.mastertech.controletarefas.persistence.Task;
import com.mastertech.controletarefas.persistence.TaskRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

public class TaskServiceTest {
    private TaskRepository taskRepository;
    private TaskService taskService;

    @BeforeEach
    public void prepare(){
        taskRepository = Mockito.mock(TaskRepository.class);
        taskService = new TaskService(taskRepository);
    }

    @Test
    public void shouldListAllProjectTasks(){
        Project project = new Project();
        project.setId(1);
        project.setName("Blog");

        Task task = new Task();
        task.setId(1);
        task.setName("Criar novo post");
        task.setProject(project);

        Mockito.when(taskRepository.findAllByProjectId(1)).thenReturn(List.of(task));

        List<Task> foundTasks = taskService.getAllByProject(1);

        Assertions.assertEquals(1, foundTasks.size());
        Assertions.assertEquals(1, task.getId());
    }

    @Test
    public void shouldCreateATask(){
        Project project = new Project();
        project.setId(1);
        project.setName("Blog");

        Task task = new Task();
        task.setId(1);
        task.setName("Criar novo post");
        task.setProject(project);

        Mockito.when(taskRepository.save(task)).thenReturn(task);

        Task createdTask = taskService.create(task);

        Assertions.assertEquals(task, createdTask);
    }
}
