package com.mastertech.controletarefas.web;

import com.mastertech.controletarefas.App;
import com.mastertech.controletarefas.persistence.Task;
import com.mastertech.controletarefas.service.TaskService;
import com.mastertech.controletarefas.web.dto.TaskPayload;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class AnotherTaskControllerTest {
    @Autowired
    private TaskController taskController;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TaskService taskService;

    @Test
    public void shouldListAllProjectTasks() throws Throwable {
        Task task = new Task();
        task.setId(1);
        task.setName("Criar post");
        task.setStatus("To Do");

        Mockito.when(taskService.getAllByProject(1)).thenReturn(List.of(task));

        mockMvc.perform(MockMvcRequestBuilders
                    .get("/projeto/1/tarefa"))
                .andExpect(MockMvcResultMatchers
                    .jsonPath("[0].id")
                    .value(1));
    }

    @Test
    public void shouldCreateATask(){
        TaskPayload payload = new TaskPayload();
        payload.setId(1);
        payload.setName("Criar post");
        payload.setStatus("To Do");

        Task taskFromPayload = payload.buildEntity();

        Task anyTask = Mockito.any(Task.class);
        Mockito.when(taskService.create(anyTask)).thenReturn(taskFromPayload);

        TaskPayload createdTask = taskController.create(1, payload);

        Assertions.assertEquals(createdTask.getId(), payload.getId());
        Assertions.assertEquals(createdTask.getName(), payload.getName());
        Assertions.assertEquals(createdTask.getStatus(), payload.getStatus());
    }
}
